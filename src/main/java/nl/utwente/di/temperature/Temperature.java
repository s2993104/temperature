package nl.utwente.di.temperature;

import java.io.*;
import jakarta.servlet.*;
import jakarta.servlet.http.*;



public class Temperature extends HttpServlet {
 	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Converter quoter;
	
    public void init() throws ServletException {
    	quoter = new Converter();
    }	
	
  public void doGet(HttpServletRequest request,
                    HttpServletResponse response)
      throws ServletException, IOException {

    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    String title = "Temperature";
    
    // Done with string concatenation only for the demo
    // Not expected to be done like this in the project
    out.println("<!DOCTYPE HTML>\n" +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                		"</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +              
                "  <P>Celsius: " +
                   request.getParameter("temperature") + "\n" +
                "  <P>Fahrenheit: " +
                   quoter.getFahrenheit(request.getParameter("temperature")) +
                "</BODY></HTML>");
  }
}
//answer to first question section 6: the doGet function will set the response to a http code which will show in screen the isbn asked for and the price related to that isbn
//answer to second question section 6: it gives information such as the xml version being used and the encoding used, the servlet to be used for which urls
