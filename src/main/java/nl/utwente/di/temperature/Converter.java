package nl.utwente.di.temperature;

public class Converter {
    public double getFahrenheit(String celsiusString){
        double celsius = Double.parseDouble(celsiusString);
        return (celsius * 1.8) + 32;
    }
}
