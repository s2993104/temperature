package nl.utwente.di.temperature;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/** * test the Converter */
public class TestConverter {

    @Test
    public void testTemperature() throws Exception {
        Converter converter = new Converter();
        double price = converter.getFahrenheit("0");
        Assertions.assertEquals(32.0, price, 0.0, "conversion from 0ºc to fahrenheit");
    }
}
